const c = 3.7795
export default {
  name: null,
  number1: null,
  number2: null,
  x: 0,
  y: 0,
  height: 80 * c,
  padding: 20 * c,
  length: 220 * c,
  dimensions: {h: 80 * c, l: 220 * c},
  draw: null,
  fonts: [],
  holesRadius: 20 * c,
  holesVerticalPos: 0,
  hole1Offset: 10 * c,
  hole2Offset: 190 * c,

  init (options) {
    this.draw = options.draw
    this.x = options.x
    this.y = options.y
    this.fonts = options.fonts

    this.holesVerticalPos = (this.y + (this.height / 2 - this.holesRadius / 2))
  },
  getSingleHeightSpace () {
    return this.height + this.padding
  },
  generate (data) {
    this.holesVerticalPos = (this.y + (this.height / 2 - this.holesRadius / 2))
    console.log(this.y)
    this.name = data[0]
    this.number1 = data[1]
    this.number2 = data[2]

    this.draw.rect(this.length, this.height).attr({
      fill: 'none',
      stroke: '#FF0000',
      x: this.x,
      y: this.y
    }).radius(this.height / 2, this.height / 2)

    let numText = this.draw.text(this.number1).font('family', this.fonts[1]).font('size', 20 * c)
    let numPosX = (this.length / 2) - (numText.length() / 2)

    numText.move(numPosX, this.y + (50 * c))

    let nameText = this.draw.text(this.name).font('family', this.fonts[0]).font('size', 18 * c)
    nameText.move((this.length / 2) - (nameText.length() / 2), this.y + (10 * c))

    if (this.number2 && this.number2.length > 0) {
      numText.move(numPosX, this.y + (36 * c))
      this.draw.line(numPosX, this.y + (58 * c), numPosX + numText.length(), this.y + (58 * c)).stroke({width: 1})

      this.number2 = this.draw.text(this.number2).font('family', this.fonts[2]).font('size', 20 * c)
      this.number2.move(numPosX, this.y + (58 * c))
    }

    this.draw.circle(this.holesRadius).attr({fill: 'none', stroke: '#FF0000'}).move(this.hole1Offset, this.holesVerticalPos)
    this.draw.circle(this.holesRadius).attr({fill: 'none', stroke: '#FF0000'}).move(this.hole2Offset, this.holesVerticalPos)
    this.y = this.y + this.height + 20
  }
}

const c = 3.7795
const d = 100 * c

export default {
  name: null,
  dimensions: {l: d, h: d},
  init (options) {
    this.draw = options.draw
    this.x = options.x
    this.y = options.y
  },
  generate (data) {
    let name = data[0]
    this.draw.text('Hei hian').move(40 * c, this.y + 20)

    let n = this.draw.text(name).font('family', 'Dancing Script').font('size', 24)
    n.move((d / 2) - (n.length() / 2), this.y + 50)

    this.draw.text('hi kum 2017 Sunday School a kalkim. ... ...').move(10 * c, this.y + 90)
    this.draw.circle(d).attr({fill: 'none', stroke: '#FF0000'}).move(0, this.y)
    this.y = this.y + d + (5 * c)
  }
}

const c = 3.7795
export default {
  number: null,
  x: 0,
  y: 0,
  height: 40 * c,
  padding: 5 * c,
  length: 220 * c,
  dimensions: {h: 40 * c, l: 220 * c},
  draw: null,
  fonts: [],
  holesRadius: 20 * c,
  holesVerticalPos: 0,
  hole1Offset: 10 * c,
  hole2Offset: 190 * c,

  init (options) {
    this.draw = options.draw
    this.x = options.x
    this.y = options.y
    this.fonts = options.fonts

    this.holesVerticalPos = (this.y + (this.height / 2 - this.holesRadius / 2))
  },
  getSingleHeightSpace () {
    return this.height + this.padding
  },
  generate (data) {
    this.holesVerticalPos = (this.y + (this.height / 2 - this.holesRadius / 2))
    this.number = data[0]

    let numText = this.draw.text(this.number).font('family', this.fonts[0]).font('size', 20 * c)
    let numPosX = (this.length / 2) - (numText.length() / 2)
    let numPosY = this.y + (this.height / 2) - 40
    // this.draw.line(numPosX, this.y, numPosX + numText.length(), this.y + numText.height() / 2).stroke({width: 1}) // testing

    numText.move(numPosX, numPosY)

    this.draw.rect(this.length, this.height).attr({
      fill: 'none',
      stroke: '#FF0000',
      x: this.x,
      y: this.y
    }).radius(this.height / 2, this.height / 2)

    this.draw.circle(this.holesRadius).attr({fill: 'none', stroke: '#FF0000'}).move(this.hole1Offset, this.holesVerticalPos)
    this.draw.circle(this.holesRadius).attr({fill: 'none', stroke: '#FF0000'}).move(this.hole2Offset, this.holesVerticalPos)
    this.y = this.y + this.height + 20
  }
}

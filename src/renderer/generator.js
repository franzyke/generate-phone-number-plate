import numberPlate from './shapes/numberPlate1'
import numberPlateSingleLine from './shapes/numberPlate2'
import circularTest from './shapes/circularTest'
import svg from 'svg.js'
import {remote} from 'electron'
import fs from 'fs'
const dialog = remote.dialog
export default {
  draw: null,
  x: 0,
  y: 0,
  fonts: [],
  phoneNumbers1: null,
  phoneNumbers2: null,
  figures: [],
  ready: false,
  saved: false,
  names: null,
  generators: {
    numberPlateSingleLine,
    numberPlate,
    circularTest
  },
  save () {
    dialog.showSaveDialog({defaultPath: remote.app.getPath('documents') + '/export.svg'}, fileName => {
      if (!fileName) {
        return
      }
      let content = this.draw.svg()
      fs.writeFile(fileName, content, (err) => {
        if (err) {
          alert('An error ocurred creating the file ' + err.message)
        } else {
          this.saved = true
        }
      })
    })
  },
  render (data, type, fonts) {
    this.fonts = fonts
    this.ready = false
    this.saved = false

    let info = data.split('\n')
    let generator = this.generators[type]
    let dimensions = generator.dimensions
    let length = dimensions.l
    let height = dimensions.h
    this.draw = svg('draw').size(length, (height + 10 * 3.7795) * info.length)
    generator.init({draw: this.draw, x: this.x, y: this.y, fonts})

    info.forEach(line => {
      generator.generate(line.split('\t'))
    })
    this.ready = true
  },
  clear () {
    if (this.draw) {
      this.draw.clear()
      this.draw.size(0, 0)
    }
    this.y = 0
    this.ready = false
    this.saved = false
  }
}
